<?php

class TV_Productdetail_Block_Index extends Mage_Core_Block_Template {

    protected function _getProductCollection() {
        $productID = $this->getProductId();
        $_product = Mage::getModel('catalog/product')->load($productID);
        return $_product;
    }

}
